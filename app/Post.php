<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
   protected $table="post";
   protected $primaryKey="id";
   protected $fillable=['id','username','email','name','role_id']; //
}
